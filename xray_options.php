<?php
// fichier d'options SPIP principal du plugin xray
// 		xray/xray_options.php
//
if (!defined('_ECRIRE_INC_VERSION')) return;

if (!defined('_ECRIRE_INC_VERSION')) return;

// Les fonctions et inclusions ne sont pas bien rangées ni optimisées
include_spip('inc/xray_options');   // éventuelle définition des options par un autre plugin
include_spip('inc/xray_options_default');

function xray_stats_global(): string {
	$mem = apcu_sma_info();
	$mem_size         = $mem['num_seg'] * $mem['seg_size'];
	$mem_avail        = $mem['avail_mem'];
	include_spip('inc/filtres');
	return ('Free mem : '.textebrut(taille_en_octets($mem_avail)) . ($mem_size ? sprintf(" (%.1f%%) ", $mem_avail * 100 / $mem_size) : ''));
}

/**
 * @param string $motif_connu       motif du vidage ou vide
 *
 * La fonction peut être utilisée avec un motif_connu pour enregistrer le motif dans le marqueur quand on vide le cache
 * ou sans motif_connu pour détecter un éventuel vidage involontaire par exemple suite à saturation de la mémoire allouée au cache
 * Doit donc être accessible partout.
 *
 * Peut-on l'appeler dans xray pour détecter un var_mode=recalcul ? non il semble que ce soit appelé AVANT le vidage de cache
 */
function xray_detecte_vidage(string $motif_connu='Flush système ?') {   // détecter les vidages de caches yc par saturation de l'espace dispo
	$Memoization = memoization();
	if (!$Memoization->exists('xray_detecte_vidage')) {
		$Memoization->set('xray_detecte_vidage', date ('d/m/Y H:i:s')." $motif_connu // re-création du marqueur car il n’existait plus");
		spip_log ("xray détecte qu'un vidage du cache a eu lieu ($motif_connu). ".xray_stats_global(), 'xray_detecte_vidage'._LOG_INFO);
	}
}

if (isset($_GET['var_mode']) and ($_GET['var_mode'] == 'recalcul')) {
	xray_detecte_vidage('var_mode=recalcul // Détecté par XRay');
}

if (!isset($_GET['exec']) or ($_GET['exec']!='xray')) {
	return;
}

/**
 * @param $faire
 * @param string $type
 * @param int $id
 * @param null $qui
 * @param null $opt
 * @return bool
 */
function autoriser_xray_dist($faire, $type='', $id=0, $qui = NULL, $opt = NULL) : bool {
	return autoriser('webmestre')
		or (
			defined ('ID_AUTEUR_AUTORISER_XRAY') 
			and ID_AUTEUR_AUTORISER_XRAY	
			and (($GLOBALS['visiteur_session']['id_auteur'] ?? 0) == ID_AUTEUR_AUTORISER_XRAY)
		);
}

/**
 * @param string $t
 * @param string $session
 * @return string
 * - Employé comme filtre dans le squelette xray_marqueur_session, qui transmet le texte élaboré $t :
 *      et associe $t à la session courante dans un tableau json_encodé dans le cache 'xray_marqueur_session'
 *      Il renvoie une chaine vide pour le html, si bien que le html caché est invisible
 * - Employé en PHP pour récupérer le texte associé à une session (sans $t, renvoie $t) 
 */
function xray_marqueur_session(string $t, string $session='') : string {
	spip_log ('xray_marqueur_session ('.str_replace("\n",' / ', $t)." , $session)\n", 'xray_marqueur_session'._LOG_DEBUG);
	$t = trim($t);
	$m = memoization();
static $sessions = null;
	if (is_null($sessions)) {
		$sessions = $m->get('xray_marqueur_sessions');
		if (!$t and !$sessions) { // rien à mémoriser et pas de squelette marqueur ou rien de mémorisé encore 
			return '';
		}
		$sessions = json_decode($sessions, true);
	}
	if ($t) {
		$sessions[spip_session()] = $t;
		$m->set('xray_marqueur_sessions', json_encode($sessions));
		return '';
	}
	if($session) {
		return $sessions[$session] ?? 'inconnu';
	}
	spip_log ("Manque session pour xray_memorise_session($t, $session", 'ERREUR_xray'._LOG_ERREUR);
	return '???';
}

$methode = $fexists = '';
$cfg = @unserialize($GLOBALS['meta']['memoization']);
$err = '';
if (!memoization() or !$cfg or !function_exists('memoization_methode')) {
	$err = "<a href='?exec=configurer_memoization'>Pour XRay, activez memoization par apc ou apcu</a>";
}
elseif (!memoization_methode('apc') and !memoization_methode('apcu')) {
	$err = "Le plugin XRay nécessite d'activer le plugin memoization avec APC ou APCu";
}
else {
	$methode = memoization_methode('apcu') ? 'apcu' : 'apc';
	$fexists = $methode.'_exists';
	if (!function_exists($fexists)) {
		$err = "Memoization est activée avec $methode, mais il manque la fonction $fexists";
	}
}

if ($err) {
	echo "<h1>$err</h1>";
	spip_log($err, 'xray');
	exit;
}

include_once ('xray_apc.php');

exit;
