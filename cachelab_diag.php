<style>
	.instructions li, .instructions p {font-size: 0.8em}
	.instructions b {background-color: #AAA}
</style>
<h1>Essayer cachelab_cibler</h1>
<div class="instructions">
<p>Instructions : Ajouter <i>à la main</i> des arguments supplémentaires à l'url, pour spécifier quelle action appliquer sur quels caches.<br>
<b>En gras</b> : la valeur par défaut.<br>
Les arguments d'url ont souvent le même nom qu'une clé des actions ou conditions de cachelab_cibler, mais `oubien` est spécifique à cette page de test, et `id_objet` est remplacé ici par `val_cle` en raison des restrictions imposées par l'écran de sécurité. 
</p>

<ul>
<li>action : 'del', 'mark', 'pass', '<b>list</b>'</li>
<li>chemin : liste de morceaux de chemins séparés par | , ou expression régulière si methode=regexp</li>
<li>methode : fonction de détection du chemin spécifié : <b>strpos</b> ou regexp ou equal (le paramètre de cachelab_cibler est <i>methode_chemin</i>)</li>
<li>partie_chemin : '<b>tout</b>' ou 'dossier' ou 'fichier' : la partie du chemin qui est testée</li>
<li>cle_objet : clé cible dans le contexte (sinon c'est 'id_'+objet)</li>
<li>val_cle : valeur cible de cle_objet OU liste des valeurs cibles séparées par des | (ex: 23|34|45). Sert pour la condition 'id_objet' de `cachelab_cibler`</li>
<li>oubien : si défini, la valeur indiquée est recherchée dans le chemin en conjonction inclusive (OR) avec les autres filtrages</li>
<li>clean : 'oui' pour effacer les caches périmés (sinon cette page n'y touche pas)</li>
</ul>
</div>
	
<?php
// echo '<pre>'.print_r($_GET,1).'</pre>';

$session = $_GET['session'] ?? '';
$cachelab_methode_chemin = $_GET['methode'] ?? 'strpos';
$chemin = ($_GET['chemin'] ?? '');
$cachelab_partie_chemin = $_GET['partie_chemin'] ?? 'tout';

echo '<xmp>'.print_r($_GET,1).'</xmp>'; // désuet mais charmant

// TODO utiliser API spip 
$cle_objet = ($_GET['cle_objet'] ?? '');
$val_cle = ($_GET['val_cle'] ?? '');
if ($cle_objet and !$val_cle) {
	echo "<b style='color:darkred'>En l'absence de 'val_cle', 'cle_objet' est ignoré</b><br>";
	$cle_objet = '';
}
$objet = ($_GET['objet'] ?? '');
if (!$objet) {
	if (strpos($cle_objet ?? '', 'id_')===0) {
		$objet = substr ($cle_objet, 3);
	}
	else {
		$objet = (defined ('XRAY_OBJET_SPECIAL') ? XRAY_OBJET_SPECIAL : '');
	}
}
$url_objet = ($val_cle and $objet) ? "?page=$objet&$cle_objet=$val_cle" : '';
$action = $_GET['action'] ?? 'list';
$clean = $_GET['clean'] ?? '';

$contexte_test=array('id_article' => 1 , 'id_rubrique' => 48 );
$contexte = ((isset ($_GET['contexte'])) ? $contexte_test : '');

$conditions = array('session'=>$session, 'chemin'=>$chemin, 'cle_objet'=>$cle_objet, 'id_objet'=>$val_cle, 'contexte'=>$contexte);

if (isset ($_GET['oubien']) and $_GET['oubien']) {
	if (!$session and !$chemin and !$cle_objet and !$contexte) {
		$conditions = ['chemin'=>$_GET['oubien']];
	}
	$conditions = [
		'ou' => [   $conditions,
					['chemin' => $_GET['oubien']]
				]
	];
}

$options = [
	'list'=>true,
	'methode_chemin'=>$cachelab_methode_chemin,
	'partie_chemin'=>$cachelab_partie_chemin,
	'clean' => $clean
];

echo '<pre>'
	.preg_replace(
		'/^Array/', '<b>Filtrage réalisé</b> : cachelab_cibler',
		print_r(['action'=>$action, 'conditions'=>$conditions, 'options'=>$options], 1)
	) . '</pre>';

$stats = cachelab_cibler(
	$action,
	$conditions,
	$options
);

$l_cible = $stats['l_cible'];
unset($stats['l_cible']);

echo   "<h3>Bilan du filtrage</h3><br>
		<br><b>Stats :</b><pre>    ".trim(str_replace('Array', '', print_r($stats, 1)), "() \n")."</pre>";

/**
 * @param string $cle
 * @return string
 */
function xray_lien_cache (string $cle=''): string {
	$joliecle = substr($cle, strpos($cle,':cache:')+7);
	return "<a href ='/ecrire/index.php?exec=xray&SCOPE=A&COUNT=20&TYPECACHE=ALL&ZOOM=TEXTECOURT&EXTRA=&WHERE=&OB=2&S_KEY=H&SORT=D&SEARCH=$joliecle&SH=".md5($cle)."'>
		$joliecle
	</a>";
}

if (count($l_cible)) {
	echo "<h3>Caches ciblés : ".count($l_cible)."</h3>
		<ul>";
	$Memoization = memoization();
	echo '_CACHE_NAMESPACE : '._CACHE_NAMESPACE;
	foreach ($l_cible as $cle)  {
		echo "<li>".xray_lien_cache($cle);
		$cle_sans_ = rtrim($cle, '_');
		if ($cle_sans_ != $cle) {
			$clememo_ = substr($cle, strlen(_CACHE_NAMESPACE));
			$clememo = rtrim($clememo_,'_');
			echo " (sans _ : ".xray_lien_cache($cle_sans_).") ";
			$v_ = $Memoization->get($clememo_);
			if (!$v_) {
				echo " (v_ vide) ";
			}
			$v = $Memoization->get($clememo);
			if (!$v) {
				echo " (v vide) ";
			}
		}
		echo "</li>";
	}
	echo "</ul>";
}
else {
	echo "Pas de cache cible<br>";
}