<?php

include_fichiers_fonctions(); // pour forcer le chargement des fonctions de spip_bonux

if (!function_exists ('filtre_slugify_dist')) {
	/**
	 * FIXME le core de SPIP 4.0 fournit identifiant_slug
	 *
	 * Copie de spip_bonux v3.5.4
	 *
	 * Cette fonction permet de transformer un texte clair en nom court pouvant servir d'identifiant, class, id, url...
	 * en ne conservant que des caracteres alphanumeriques et un separateur
	 *
	 * @param string $texte
	 *   Texte à transformer en nom machine
	 * @param string $type
	 *
	 * @param array $options
	 *   string separateur : par défaut, un underscore `_`.
	 *   int longueur_maxi : par defaut 60
	 *
	 * @return string
	 */
	function filtre_slugify_dist (string $texte, string $type = '', array $options = []): string {

		$separateur = ($options['separateur'] ?? '_');
		$longueur_maxi = ($options['longueur_maxi'] ?? 60);

		if (!function_exists ('translitteration')) {
			include_spip ('inc/charsets');
		}

		// pas de balise html
		if (strpos ($texte, '<') !== false) {
			$texte = strip_tags ($texte);
		}
		if (strpos ($texte, '&') !== false) {
			$texte = unicode2charset ($texte);
		}
		// On enlève les espaces indésirables
		$texte = trim ($texte);

		// On enlève les accents et cie
		$texte = translitteration ($texte);

		// On remplace tout ce qui n'est pas un mot par un separateur
		$texte = preg_replace (',\W+,ms', $separateur, $texte);

		// nettoyer les doubles occurences du separateur si besoin
		while (strpos ($texte, "$separateur$separateur") !== false) {
			$texte = str_replace ("$separateur$separateur", $separateur, $texte);
		}

		// pas de separateur au debut ni a la fin
		$texte = trim ($texte, $separateur);

		// en minuscules
		$texte = strtolower ($texte);

		switch ($type) {
			case 'class':
			case 'id':
			case 'anchor':
				if (preg_match (',^\d,', $texte)) {
					$texte = substr ($type, 0, 1) . $texte;
				}
		}

		if (strlen ($texte) > $longueur_maxi) {
			$texte = substr ($texte, 0, $longueur_maxi);
		}

		return $texte;
	}
}

if (!function_exists('slugify')) {
	function slugify($texte, $type = '', $options = array()) {
		$slugify = chercher_filtre('slugify');
		return $slugify($texte, $type, $options);
	}
}


// copie un peu modifiée de la fonction définie dans public/cacher.php
if (!function_exists('gunzip_page')) {
	function gunzip_page(&$page) {
		if (isset ($page['gz']) and $page['gz']) {
			$page['texte'] = gzuncompress($page['texte']);
			$page['gz'] = false; // ne pas gzuncompress deux fois une meme page
		}
	}
}
else {
	die("La fonction gunzip_page ne devrait pas être déjà définie"); // à défaut de disposer de la lib nobug
}

if (!function_exists('cache_est_sessionne')) {
	function cache_est_sessionne ($nomcache): string {    
		if (preg_match (XRAY_PATTERN_SESSION_AUTH, $nomcache)) {
			return 'session_auth';
		}
		elseif (preg_match (XRAY_PATTERN_SESSION_ANON, $nomcache)) {
			return 'session_anon';
		}
		else {
			return '';
		}
	}
}
if (!function_exists('cache_est_talon')) {
	function cache_est_talon (string $nomcache, &$data = ''): ?bool {
		if (preg_match (XRAY_PATTERN_SESSION, $nomcache)) {
			return false;
		}
		if (!is_array ($data)) { // textwheels par exemple
			return null;
		}
		return !isset($data['contexte']);
	}
}
if (!function_exists('cache_est_dynamique')) {
	function cache_est_dynamique (string $nomcache, &$data = '') {
		if (!is_array ($data) or !isset($data['process_ins'])) {// textwheels par exemple
			return null;
		}
		return $data['process_ins'] == 'php';
	}
}
if (!function_exists('cache_est_statique')) {
	function cache_est_statique (string $nomcache, &$data = '') {
		if (!is_array ($data) or !isset($data['process_ins'])) {// textwheels par exemple
			return null;
		}
		return $data['process_ins'] == 'html';
	}
}
if (!function_exists('cache_est_page')) {
	function cache_est_page ($nomcache, &$data = '') {
		if (!is_array ($data)) { // textwheels par exemple
			return null;
		}
		// On délègue à une fonction à définir pour chaque site car ça dépend du format des urls
		if (function_exists ('xray_reconnaitre_si_cache_est_page')) {
			return xray_reconnaitre_si_cache_est_page ($nomcache, $data);
		}
		// Par défaut on utilise une heuristique qui couvre pas mal de cas :
		// les pages spip.php, les pages sans spip.php et les urls raccourcies /1234
		if (!$nomcache) {
			return false;
		}
		$rev_nomcache = strrev ($nomcache);
		return
			(stripos ($rev_nomcache, strrev ('/spip')) === 0)
			or ($rev_nomcache[0] === '/')
			or preg_match ("~/\d+$~", $nomcache);
	}
}

/* Exemple : définition adaptée pour les urls historiques en "article.php?id_article="
function xray_reconnaitre_si_cache_est_page ($nomcache, $data) {
	$len = strlen($nomcache);
	foreach (['/spip', 'article/article', 'rubrique/rubrique'] as $pattern) {
		$pos = strrpos ($nomcache, $pattern);
		$minpos = $len - strlen ($pattern) -1; // parfois il y a un _ en plus à la fin
		if (($pos !== false) and ($pos >= $minpos)) {
			return true;
		}
	}
	return false;
	}
*/

if (!function_exists('cache_est_perime')) {
	// Fonctions également définies par cachelab

	// Signature moche requise par le cadre d'usage dans XRay... à revoir
    /**
     * @param string $nomcache
     * @param &$spip_cache_unused
     * @param &$apcu_entry
     * @return bool                 Le cache est il périmé ?
     */
    function cache_est_perime (string $nomcache, &$spip_cache_unused='', &$apcu_entry): bool {
		return (bool) cache_est_perime_explic ($nomcache, $apcu_entry);
	}

    /**
     * @param string $nomcache
     * @param &$data
     * @return array       [raison de la péremption, précisions pour l'explication]
     * 
     *  Teste la péremption et renvoie un tableau d'explication ou un tableau vide sinon
     */
    function cache_est_perime_explic (string $nomcache, &$data=''): array {
	global $meta_derniere_modif;
	static $time = 0;
		if (!apcu_exists($nomcache)) {
			return ['N’existe plus pour APCu', 'Ça suffit !']; 
		}
		$time = ($time ?: time());
		$explic_meta = 'meta_dernière_modif='.date(DATE_FORMAT, $meta_derniere_modif);
		if ($data['ttl'] and ($data['creation_time'] + $data['ttl'] <= $time)) {
			$explic_apcu = 'Péremption APCU='.date(DATE_FORMAT,$data['creation_time'] + $data['ttl']);
			if ($data['creation_time'] < $meta_derniere_modif) {
				return [
					"Périmé / APCu et SPIP",
					"$explic_apcu ; $explic_meta"
				];
			}
			return [
				"Périmé / APCu",
				$explic_apcu
			];
		}
		if ($data['creation_time'] <  $meta_derniere_modif) {
			return ['Périmé / SPIP', $explic_meta];
		}
		return [];
	}
}

if (!function_exists('cache_est_squelette')) {
	function cache_est_squelette (string $nomcache): bool {
		return stripos ($nomcache, _CACHE_NAMESPACE . 'cache:') === 0;
	}
}