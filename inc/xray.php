<?php

/**
 * @param array &$cache info sur les caches récupérés de apcu
 * @param string $tri   colonne selon laquelle on trie
 * @param string $asc_or_desc   'A' ou 'D' selon qu'on croit ou décroît
 * @return array        liste des caches triés
 */
function xray_sort_cache_list (array $cache, string $tri, string $asc_or_desc): array {
	$list = [];
	// TODO : trier aprés avoir filtré, comme pour les squelettes
	// builds list with alpha numeric sortable keys
	foreach ($cache['cache_list'] as $entry) {
		switch ($tri) {
			case 'S':
				if (isset($entry['source'])) {
					$k = sprintf ('%015d-', $entry['source']);
				}
				else {  // Pas un cache spip
					$k= 'zzz-';
				}
				break;
			case 'A':
				$k = sprintf ('%015d-', $entry['access_time']);
				break;
			case 'H':
				$k = sprintf ('%015d-', $entry['num_hits']);
				break;
			case 'Z':
				$k = sprintf ('%015d-', $entry['mem_size']);
				break;
			case 'C':
				$k = sprintf ('%015d-', $entry['creation_time']);
				break;
			case 'T':
				$k = sprintf ('%015d-', $entry['ttl']);
				break;

			// mode "affichage des squelettes" :
			case 'Q':  // par nom de squelettes
			case 'N':  // par nb de caches par squelette
			case 'V':  // par taille des caches par squelette
			default :
				return $cache['cache_list']; // on triera plus tard
		}
		$list[$k . $entry['info']] = $entry;
	}
	// sort list
	switch ($asc_or_desc) {
		case "A":
			ksort ($list);
			break;
		case "D":
			krsort ($list);
			break;
		default:
			echo "...ah ben non pas de tri.";
			break;
	}
	return $list;
}

/**
 * @param array $liste_squelettes
 * @param string $tri               colonne selon laquelle trier :
 *                                  Q : le chemin du squelette, N : nb de cache par squelette, V : taille mémoire des caches par squel
 * @param string $asc_or_desc
 * @return array
 */
function xray_sort_squelettes (array &$liste_squelettes, string $tri, string $asc_or_desc): array {
	$indexes = ['Q'=>'source', 'N'=>'nb', 'V'=>'mem_size', 'I'=>'num_hits'];
	$index = ($indexes[$tri] ?? $indexes['N']);
	usort (
		$liste_squelettes,
		function ($s1, $s2) use ($index, $asc_or_desc) {
			return ($asc_or_desc==='A') ? ($s1[$index] <=> $s2[$index]) : ($s2[$index] <=> $s1[$index]);
		}
	);
	return $liste_squelettes;
}

/**
 * @param array $liste_sessions
 * @param string $tri               colonne selon laquelle trier :
 *      S : session, N : nb de cache pour cette session, Z : taille mémoire des caches de cette session, H : nb hits des caches
 * @param string $asc_or_desc
 * @return array
 */
function xray_sort_sessions (array &$liste_sessions, string $tri, string $asc_or_desc): array {
	$indexes = ['S'=>'session', 'N'=>'nb', 'Z'=>'mem_size', 'H'=>'num_hits'];
	$index = ($indexes[$tri] ?? 'nb');
	uasort (
		$liste_sessions,
		function ($s1, $s2) use ($index, $asc_or_desc) {
			return ($asc_or_desc==='A') ? ($s1[$index] <=> $s2[$index]) : ($s2[$index] <=> $s1[$index]);
		}
	);
	return $liste_sessions;
}

/**
 * @param string $type_liste
 *  affiche l'entête du tableau des résultats du scanner
 */
function xray_scanner_head (string $type_liste) {
	echo '<div class="info">
				<table cellspacing=0>
					<tbody><tr>';
	switch ($type_liste) {
	case 'squelettes' : 
		echo '<th align="left">', sortheader ('Q', 'Squelette') . '</th>
				  <th align="left">Compilé</th>
				  <th align="left">', sortheader ('N', 'Nb Caches') . '</th>
				  <th align="left">', sortheader ('I', 'Hits') . '</th>
				  <th align="left">', sortheader ('V', 'Taille') . '</th>';
		break;
	case 'caches' :
		echo '<th align="left">Caches - ', sortheader ('S', 'tri par Squelette') . '</th>',
		'<th>', sortheader ('H', 'Hits'), '</th>',
		'<th>', sortheader ('Z', 'Size'), '</th>',
		'<th>', sortheader ('C', 'Created at'), '</th>',
		'<th>', sortheader ('T', 'Timeout'), '</th>',
		'<th>Del</th>
				</tr>';
		break;
	case 'sessions' :
		echo '<th align="left">', sortheader ('S', 'Sessions') . '</th>
			  <th align="left">Marqueurs</th>
			  <th align="left">', sortheader ('N', 'Nb Caches') . '</th>
			  <th align="left">', sortheader ('H', 'Hits') . '</th>
			  <th align="left">', sortheader ('Z', 'Taille') . '</th>';
		break;
	}
}

/**
 * @param string $search        expression recherchée
 * @param string $where         lieu de recherche
 * @return string               expression recherchée préparée pour la recherche
 */
function xray_prepare_recherche (string $search, string $where) : string {
	if (!$search) {
		return '';
	}
	// Don't use preg_quote because we want the user to be able to specify a
	// regular expression subpattern.
	// FIXME : certaines regexp ne sont pas comprises. Depuis la syntaxe simlifiée ?
	// Detection of a potential preg error :
	if (@preg_match ('~' . xray_echappe_recherche ($search) . '~i', null) === false) {
		echo '<div class="error">Error: search expression is not a valid regexp (it needs escaping parentheses etc)</div>';
		$search = preg_quote ($search, '/');
		echo "<div class='warning'>
					Warning : search expression has been preg_quoted :
						<xmp>$search</xmp>
				</div>";
	}
	// Syntaxe simplifiée pour la recherche dans les contextes
	// "id_truc=val" est compris comme le "[id_truc] = val" des fournitures de print_r
	if (
		$where == 'CONTEXTE'
		and !preg_match ("/^\[.*\]/", $search)
	) {
		$search = preg_replace (
			"/^(.*)\s*=\s*(.*)$/",
			"\\[$1\\]\\s*=>\\s*$2",
			$search
		);
	}
	else {
		if (substr($search, 0, 1) != '^') {
			$search = '^.*'.$search;
		}
		if (substr($search, -1, 1) != '$') {
			$search .= '.*$';
		}
	}

	return '~' . $search . '~i';
}

/**
 * Affiche les options de recherche, filtrage et affichage du scanner
 */
function xray_menu_scanner () {
	global $MYREQUEST, $MY_SELF;
	echo '<form>
		<input type="hidden" name="OB" value="' . $MYREQUEST['OB'] . '">
		<input type="hidden" name="exec" value="' . $MYREQUEST['exec'] . '">
		<input type="hidden" name="S_KEY" value="' . $MYREQUEST['S_KEY'] . '">
		<input type="hidden" name="TYPELISTE" value="' . $MYREQUEST['TYPELISTE'] . '">
		<p style="margin-bottom:10px">
			<b style="margin-bottom:5px">Lister :</b>
			<span style="margin-left: 1em; "></span>
			<select name="COUNT" onChange="form.submit()" dir="rtl">
				<option value=10 ', $MYREQUEST['COUNT'] == '10' ? ' selected' : '', ' style="text-align: right">10</option>
				<option value=20 ', $MYREQUEST['COUNT'] == '20' ? ' selected' : '', '>20</option>
				<option value=50 ', $MYREQUEST['COUNT'] == '50' ? ' selected' : '', '> 50</option>
				<option value=100', $MYREQUEST['COUNT'] == '100' ? ' selected' : '', '>100</option>
				<option value=150', $MYREQUEST['COUNT'] == '150' ? ' selected' : '', '>150</option>
				<option value=250', $MYREQUEST['COUNT'] == '250' ? ' selected' : '', '>250</option>
				<option value=500', $MYREQUEST['COUNT'] == '500' ? ' selected' : '', '>500</option>
				<option value=0  ', $MYREQUEST['COUNT'] == '0' ? ' selected' : '', '>Tous</option>
			</select>&nbsp;&nbsp;';

	echo '<select name="TYPELISTE" onChange="form.submit()" dir="rtl">
				<option value="caches" ', $MYREQUEST['TYPELISTE'] === 'caches' ? ' selected' : '', ' style="text-align: right">caches</option>
				<option value="squelettes" ', $MYREQUEST['TYPELISTE'] === 'squelettes' ? ' selected' : '', ' style="text-align: right">squelettes</option>
				<option value="sessions" ', $MYREQUEST['TYPELISTE'] === 'sessions' ? ' selected' : '', ' style="text-align: right">sessions</option>
			</select>&nbsp;&nbsp;';

	$extra_visibility = 'visible';
	if ($MYREQUEST['TYPELISTE'] != 'caches') {
		$extra_visibility = 'hidden';
	}
	echo '<span style="visibility: '.$extra_visibility.'">
			<b style="margin-left: 2em; ">Afficher :</b> 
			<select name="EXTRA"  onChange="form.submit()">
				<option value="" ', $MYREQUEST['EXTRA'] == '' ? ' selected' : '', '></option> 
				<option value=CONTEXTE ', $MYREQUEST['EXTRA'] == 'CONTEXTE' ? ' selected' : '', '>Contexte</option>
				<option value=CONTEXTES_SPECIAUX ', $MYREQUEST['EXTRA'] == 'CONTEXTES_SPECIAUX' ? ' selected' : '', '>Contextes spécifiques</option>
				<option value=HTML_COURT ', $MYREQUEST['EXTRA'] == 'HTML_COURT' ? ' selected' : '', '>HTML produit</option>
				<option value=SQUELETTE ', $MYREQUEST['EXTRA'] == 'SQUELETTE' ? ' selected' : '', '>Squelette</option>
				<option value=SKEL ', $MYREQUEST['EXTRA'] == 'SKEL' ? ' selected' : '', '>Squelette compilé</option>
				<option value=INFO_AUTEUR ', $MYREQUEST['EXTRA'] == 'INFO_AUTEUR' ? ' selected' : '', '>Infos auteur</option>
				<option value=INFO_OBJET_SPECIAL ', $MYREQUEST['EXTRA'] == 'INFO_OBJET_SPECIAL' ? ' selected' : '', '>Infos ' . XRAY_OBJET_SPECIAL . '</option>
				<option value=INVALIDEURS ', $MYREQUEST['EXTRA'] == 'INVALIDEURS' ? ' selected' : '', '>Invalideurs</option>
				<option value=INVALIDEURS_SPECIAUX ', $MYREQUEST['EXTRA'] == 'INVALIDEURS_SPECIAUX' ? ' selected' : '', '>Invalideurs spécifiques</option>
				<option value=INCLUSIONS ', $MYREQUEST['EXTRA'] == 'INCLUSIONS' ? ' selected' : '', '>&lt;INCLURE&gt;</option>
				<option value=MACROSESSIONS ', $MYREQUEST['EXTRA'] == 'MACROSESSIONS' ? ' selected' : '', '>#_SESSION</option>
				<option value=MACROAUTORISER ', $MYREQUEST['EXTRA'] == 'MACROAUTORISER' ? ' selected' : '', '>#_AUTORISER_SI</option>
			</select>
		</span>';

	if (!XRAY_NEPASAFFICHER_AUTRES_SITES) {
		// Si on veut pouvoir regarder la liste des caches des autres sites mutualisant APCu (cryptés)
		echo '<input type="checkbox" style="margin-left: 4em;"
				name="xray_afficher_autres_sites" id="xray_afficher_autres_sites" 
				'.(($_GET['xray_afficher_autres_sites'] ?? XRAY_NEPASAFFICHER_AUTRES_SITES) ? 'checked' : '').'
			><label for="xray_afficher_autres_sites">
				Voir tous les caches, pas seulement ceux de '.substr(_CACHE_NAMESPACE, 0, strpos(_CACHE_NAMESPACE, ':'))
			.'</label>';
	}
	else {
		echo '<span style="margin-left: 4em;">Préfixe exigé : '._CACHE_NAMESPACE.'</span>';
	}
	echo '</p>';
	echo '<p><b>Filtrer caches : </b> 
		<span style="margin-left: 1em; "></span>
		Types
		<select name="TYPECACHE"  onChange="form.submit()">
			<option value=ALL', $MYREQUEST['TYPECACHE'] == 'ALL' ? ' selected' : '', '>Tous</option>
			<option value="NON_SESSIONS"', $MYREQUEST['TYPECACHE'] == 'NON_SESSIONS' ? ' selected' : '', '>Non sessionnés</option>
			<option value="SESSIONS"', $MYREQUEST['TYPECACHE'] == 'SESSIONS' ? ' selected' : '', '>Sessionnés</option>
			<option value="SESSIONS_AUTH"', $MYREQUEST['TYPECACHE'] == 'SESSIONS_AUTH' ? ' selected' : '', '>Sessionnés identifiés</option>
			<option value="SESSIONS_NONAUTH"', $MYREQUEST['TYPECACHE'] == 'SESSIONS_NONAUTH' ? ' selected' : '', '>Sessionnés non identifiés</option>
			<option value="SESSIONS_TALON"', $MYREQUEST['TYPECACHE'] == 'SESSIONS_TALON' ? ' selected' : '', '>Talons de session</option>
			<option value="FORMULAIRES"', $MYREQUEST['TYPECACHE'] == 'FORMULAIRES' ? ' selected' : '', '>Formulaires</option>
			<option value="DYNAMIQUES"', $MYREQUEST['TYPECACHE'] == 'DYNAMIQUES' ? ' selected' : '', '>Dynamiques</option>
			<option value="STATIQUES"', $MYREQUEST['TYPECACHE'] == 'STATIQUES' ? ' selected' : '', '>Statiques</option>
			<option value="PAGES"', $MYREQUEST['TYPECACHE'] == 'PAGES' ? ' selected' : '', '>Pages</option>
			<option value="INCLUSIONS"', $MYREQUEST['TYPECACHE'] == 'INCLUSIONS' ? ' selected' : '', '>Inclusions</option>
			<option value="PERIMES"', $MYREQUEST['TYPECACHE'] == 'PERIMES' ? ' selected' : '', '>Périmés</option>';
	if ($MYREQUEST['TYPELISTE'] != 'squelettes') {
		echo '<option value="NON_SQUELETTES"', $MYREQUEST['TYPECACHE'] == 'NON_SQUELETTES' ? ' selected' : '', '>Non squelettes</option>';
	}
	echo '
		</select>
		<span style="margin-left: 1em; "></span>
		&nbsp;&nbsp;&nbsp;
		<span title="REGEXP">avec</span> <input name=SEARCH value="', htmlspecialchars($MYREQUEST['SEARCH'], ENT_QUOTES, 'UTF-8'), '" type=text size=25 style="line-height: 2em" />
		<b>dans</b>
		<select name=WHERE onChange="form.submit()">
			<option value="" ', $MYREQUEST['WHERE'] == '' ? ' selected' : '', '>Noms des caches</option>
			<option value="ALL" ', $MYREQUEST['WHERE'] == 'ALL' ? ' selected' : '', '>Tout le contenu</option>
			<option value="HTML" ', $MYREQUEST['WHERE'] == 'HTML' ? ' selected' : '', '>HTML produit</option>
			<option value="CONTEXTE" ', $MYREQUEST['WHERE'] == 'CONTEXTE' ? ' selected' : '', '>Contexte</option>
			<option value="FICHIER_SOURCE" ', $MYREQUEST['WHERE'] == 'FICHIER_SOURCE' ? ' selected' : '', '>Nom du squelette source</option>
			<option value="FICHIER_SKEL" ', $MYREQUEST['WHERE'] == 'FICHIER_SKEL' ? ' selected' : '', '>Nom du squelette compilé</option>
			<option value="META" ', $MYREQUEST['WHERE'] == 'META' ? ' selected' : '', '>Métadonnées</option>
			<option value="INCLUSIONS" ', $MYREQUEST['WHERE'] == 'INCLUSIONS' ? ' selected' : '', '>&lt;INCLURE&gt;</option>
			
		</select>
		&nbsp;&nbsp;&nbsp;
		<input type=submit id="ListSubmit" name="ListSubmit" value="List" 
			style="font-size: larger; padding: 0.1em 3em; background: green;"
		>';

	if (test_plugin_actif ('cachelab')) {
		echo '<input type=submit id="DelSubmit"  name="DelSubmit" value="X" style="color:red">';
	}
	else {
		echo '<span title="Installez cachelab pour pouvoir supprimer les caches de la sélection courante (et également tester les fonctions de ciblages)" style="color:grey; padding:0 2em; font-size: 120%"> ?X? </span>';
	}

	echo '</p>';
	
	if (($_GET['WHERE'] ?? '') === 'CONTEXTE') {
		echo "<i>La recherche de 'id_article=1234' ramènera les caches ayant cette valeur dans le contexte</i><br>";
	}
	echo '</form>
		';

	if (in_array ($MYREQUEST['TYPECACHE'], array('PAGES', 'INCLUSIONS'))
		and !function_exists ('xray_reconnaitre_si_cache_est_page')
	) {
		echo 'Pour reconnaître si un cache correspond à une page ou à une inclusion, il faut définir une fonction <i>xray_reconnaitre_si_cache_est_page($nomcache, $data)</i> dans le fichier de _fonctions.php de votre plugin, avec un code adapté aux types d’urls de votre site.
			<br>À défaut, la sélection présentée ici est certainement erronée.';
	}
}

/**
 * @param string $typecache            type de cache affiché
 * @param string &$pattern_typecache    regexp implémentant le filtrage des typecaches
 * @param string &$also_required        fonction à appeler en plus
 * @param bool &$also_required_bool   valeur requise pour le retour de $also_required
 *
 * Cette fonction prépare les paramètres permettant ensuite le filtrage
 */
function xray_prepare_filtrage (string $typecache ,string &$pattern_typecache, string &$also_required, bool &$also_required_bool) {
	switch ($typecache) {
		case 'ALL':
			$pattern_typecache = '';
			break;
		case 'NON_SESSIONS':
			$pattern_typecache = XRAY_PATTERN_NON_SESSION;
			$also_required = 'cache_est_talon';
			$also_required_bool = false;
			break;
		case 'SESSIONS':
			$pattern_typecache = XRAY_PATTERN_SESSION;
			break;
		case 'SESSIONS_AUTH':
			$pattern_typecache = XRAY_PATTERN_SESSION_AUTH;
			break;
		case 'SESSIONS_NONAUTH':
			$pattern_typecache = XRAY_PATTERN_SESSION_ANON;
			break;
		case 'SESSIONS_TALON':
			$pattern_typecache = XRAY_PATTERN_NON_SESSION;
			$also_required = 'cache_est_talon';
			break;
		case 'FORMULAIRES':
			$pattern_typecache = '~formulaires/~i';
			break;
		case 'DYNAMIQUES' :
			$also_required = 'cache_est_dynamique';
			break;
		case 'STATIQUES' :
			$also_required = 'cache_est_statique';
			break;
		case 'PAGES' :
			$also_required = 'cache_est_page';
			break;
		case 'INCLUSIONS' :
			$also_required = 'cache_est_page';
			$also_required_bool = false;
			break;
		case 'PERIMES' :
			$also_required = 'cache_est_perime';
			break;
		case 'NON_SQUELETTES' :
			$also_required = 'cache_est_squelette';
			$also_required_bool = false;
			break;
	}
}

/**
 * @param array|string $searched        cache complet, dont on ne cherche qu'une partie, selon $where
 * @param string &$search               chaine cherchée, modifiée dans le cas "INCLUSIONS"
 * @param string $where                 endroit où l'on cherche
 * @return string                       partie du cache qui est cherchée
 */
function xray_get_searched_part ($searched, string &$search, string $where) {
	if ($search and (substr($search,0, 2) !== '~~') and $where) {
		switch ($where) {
			case 'ALL' :
				return $searched;
			case 'HTML' :
				if (is_array ($searched)) {     // !textwheel Object
					return $searched['texte'] ?? '';
				}
				break;
			case 'CONTEXTE' :
				if (is_array ($searched)) {     // !textwheel Object
					return $searched['contexte'] ?? '';
				}
				break;
			case 'FICHIER_SOURCE' :
				if (is_array ($searched)) {     // !textwheel Object
					return $searched['source'] ?? '';
				}
				break;
			case 'FICHIER_SKEL' :
				if (is_array ($searched)) {     // !textwheel Object
					return $searched['squelette'] ?? '';
				}
				break;
			case 'META' :
				if (is_array ($searched)) {     // !textwheel Object
					unset($searched['texte']);
					return $searched;
				}
				break;
			case 'INCLUSIONS' :
				if (is_array($searched)){                   // !textwheel Object
					$search = substr ($search, 1);          // Suppr le ~ séparateur initial de la regexp
					if (substr($search,0, 3) === '^.*') {   // toujours sauf si demande foireuse
						$search = substr($search, 3);
					}
					$search = '~recuperer_fond[^,]*'.$search;// il y a déjà le ~ final
					return $searched['texte'] ?? '';
				}
				break;
			default :
				die("Mauvaise valeur pour where : '$where'");
		}
	}
	return '';
}

/**
 * @param array|string $data                 le cache
 * @param string $type_extra    le type d'extra demandé
 * @return string               le texte de l'extra à afficher
 */
function xray_prepare_extra($data, string $type_extra): string {
	$extra = null;
	$jolif='xray_joli_cache';
	
	if (!is_array($data)) {
		return '';  // Cas des "SPIPTextWheelRuleset Object". Un clic sur le nom du cache permet de voir le contenu
	}
	switch ($type_extra) {
			case 'CONTEXTE':
				$jolif='xray_joli_contexte';
				$extra = $data['contexte'] ?? '(non défini)';
				break;
				
			case 'CONTEXTES_SPECIAUX':
				if (isset($data['contexte'])) {
					$jolif='xray_joli_contexte';
					$extra = $data['contexte'];
					foreach (['lang', 'date', 'date_default', 'date_redac', 'date_redac_default'] as $ki)
						unset($extra[$ki]);
				} else
					$extra = '(non défini)';
				break;

			case 'HTML_COURT' :
				if (isset($data['texte'])) {
					$extra = xray_ajuste_longueur_html ($data['texte']);
				}
				else {
					$extra = '(non défini)';
				}
				break;

			case 'SQUELETTE' :
				$extra = isset($data['source'])
					? '</xmp>'.xray_link_to_file( 'source', $data['source']).'<xmp>'
					: '(aucune source spécifiée)';
				break;

			case 'SKEL':
				$extra = isset($data['squelette']) 
						? '</xmp>'.xray_link_to_file( 'squelette', $data['squelette']).'<xmp>'
						: '(non défini)';
				break;

			case 'INFO_AUTEUR':
				$jolif='xray_joli_contexte';
				if (isset($data['contexte'])) {
					foreach (array(
						         'id_auteur',
						         'email',
						         'nom',
						         'statut',
						         'login'
					         ) as $ki) {
						if (isset($data['contexte'][$ki])) {
							$extra[$ki] = $data['contexte'][$ki];
						}
					}
				}
				break;

			case 'INFO_OBJET_SPECIAL':
				$jolif='xray_joli_contexte';
				if (isset($data['contexte'])) {
					$ki = 'id_'.XRAY_OBJET_SPECIAL;
					if (isset($data['contexte'][$ki])) {
						$extra[$ki] = $data['contexte'][$ki];
					}
				}
				break;
				
			case 'INVALIDEURS':
				if (isset ($data['invalideurs'])) {
					$extra = $data['invalideurs'];
				}
				break;
				
			case 'INVALIDEURS_SPECIAUX':
				if (isset ($data['invalideurs'])) {
					$extra = $data['invalideurs'];
					foreach (array(
						         'cache',
						         'session'
					         ) as $ki) {
						unset($extra[$ki]);
					}
				}
				break;
				
			case 'INCLUSIONS' :
				if (!isset ($data['texte'])) {
					$extra = '(html non défini)';
				}
				elseif (preg_match_all("/<\?php\s+echo\s+recuperer_fond\s*\(\s*'([a-z0-9_\-\.\/]+)'/i", $data['texte'], $matches)) {
					$extra = $matches[1];
				}
				else {
					$extra = '(aucune <INCLUSION>)';
				}
				break;
				
			case 'MACROSESSIONS' :
				if (!isset ($data['texte'])) {
					$extra = '(html non défini)';
				}
				elseif (preg_match_all("/\bpipelined_session_get\s*\((['\"a-z0-9\s_\-\.\/,]+)\)/i", $data['texte'], $matches)) {
					$extra = $matches[1];
				}
				else {
					$extra = '(aucune balise #_SESSION ou #_SESSION_SI)';
				}
				break;
				
			case 'MACROAUTORISER' :
				if (!isset ($data['texte'])) {
					$extra = '(html non défini)';
				}
				elseif (preg_match_all("/if\s+\(autoriser\s*\((.+)\)\s*\)\s*{\s*\?>/i", $data['texte'], $matches)) {
					$extra = $matches[1];
				}
				else {
					$extra = '(aucune balise #_AUTORISER_SI)';
				}
				break;
	}
	if ($extra) {
		return $jolif($extra);
	}
	return '<span style="color:grey">rien</style>';
}