<?php

// il faut définir un inc/xray_options.php lorsqu'on include_spip ('inc/xray_stats') dans le code php d'un plugin
if (!defined('XRAY_PATTERN_STATS_SPECIALES'))
	include_spip('inc/xray_options');
if (!defined('XRAY_PATTERN_STATS_SPECIALES'))
	include_spip ('inc/xray_options_default');
if (!defined('XRAY_PATTERN_STATS_SPECIALES'))
	die ("erreur : XRAY_PATTERN_STATS_SPECIALES n'est pas défini");

/**
 * @param array|null $cache
 * @return string[]
 */
function xray_stats(array $cache=null): array {
	if (!$cache) {
		$cache = apcu_cache_info();
	}
	if (!$cache) {
		return ['etat' => 'vide'];  
	}
	$stats=['etat' => 'ok'];

	// on ordonne par date de création
	$list = [];
	// Par défaut on montre le tableau des stats pour le site courant
	// mais un argument d'url permet d'obtenir les stats globales
	$list_all = (($_GET['xray'] ?? '') == 'stats_all');  
	
	foreach($cache['cache_list'] as $entry) {
		if (!$list_all and (stripos($entry['info'], _CACHE_NAMESPACE) !== 0)) {
			continue;
		}
		$k = 'a_'.sprintf('%015d', $entry['creation_time']).$entry['info'];
		$entry ['date_crea'] = date(DATE_FORMAT, $entry['creation_time']);
		$entry ['info_exists'] = apcu_exists ($entry['info']);
		$list[$k] = $entry;
	}
	// tri à l'envers pour ne pas réindexer le tableaux numériquement avec array_shift
	krsort($list, SORT_STRING);
	
	$meta_derniere_modif = lire_config('derniere_modif');

	$stats['existent']=$stats['invalides']=$stats['speciaux']=$stats['generaux']=$stats['fantomes']
		=array(
			'nb'=>0, 
			'taille'=>0, 
			'naissance'=>0, 
			'nb_hits'=>0, 
			'nb_requetes'=>0,
			'mem_hits'=>0,
			'mem_requetes'=>0
		);
	$existent = &$stats['existent'];    // non "fantomes"
	$invalides = &$stats['invalides'];  // périmés pour spip ou apcu
	$speciaux = &$stats['speciaux'];    // Javascript et css (selon XRAY_PATTERN_STATS_SPECIALES)
	$generaux = &$stats['generaux'];    // les autres !
	$fantomes = &$stats['fantomes'];    // apcu_exists false
	$time = time();
	while (count($list)) {
		$d = array_pop($list);
		if ($d and apcu_exists($d['info'])) {
			xray_nouvelle_stat ($existent, $d);
			if ($meta_derniere_modif > $d['creation_time']
				or $d['creation_time'] + $d['ttl'] <= $time) {
				xray_nouvelle_stat ($invalides, $d);
			}
			elseif (preg_match(XRAY_PATTERN_STATS_SPECIALES, $d['info'])) {
				xray_nouvelle_stat ($speciaux, $d);
			}
			else {
				xray_nouvelle_stat ($generaux, $d);
			}
		}
		else {
			xray_nouvelle_stat ($fantomes, $d);
		}
	}
	return $stats;
}

/**
 * @param array $stat
 * @param array $d
 * @return void
 */
function xray_nouvelle_stat (array &$stat, array $d): void {
	$stat['nb']++;
	$stat['taille'] += $d['mem_size'];
	$stat['nb_hits'] += $d['num_hits'];
	$stat['nb_requetes'] += $d['num_hits'] + 1;
	$stat['mem_hits'] += $d['mem_size'] * $d['num_hits'];
	$stat['mem_requetes'] += $d['mem_size'] * ($d['num_hits'] + 1);
	if (!$stat['naissance'] or ($stat['naissance'] > $d['creation_time'])) {
		$stat['naissance'] = date (JOLI_DATE_FORMAT, $d['creation_time']);
	}
}

/**
 * @param array $stats
 * @param string $what
 * @param string $label
 * @return string
 */
function xray_stats_print(array &$stats, string $what, string $label): string {
	if (!($stats[$what]['nb'] ?? 0)) {
		return "<tr><td colspan=2>$label : aucun cache</td></tr>";
	}
	return "
		<tr><td colspan=2><b>$label</b></td></tr>
		<tr class=tr-0><td class=td-0>Nb caches</td><td>{$stats[$what]['nb']}</td></tr>
		<tr class=tr-0><td class=td-0>Taille totale</td><td>".taille_en_octets($stats[$what]['taille'])."</td></tr>
		<tr class=tr-0><td class=td-0>Nb requetes</td><td>{$stats[$what]['nb_requetes']}</td></tr>
		<tr class=tr-0><td class=td-0>Nb hits</td>
			<td title='".round(100*$stats[$what]['mem_hits']/$stats[$what]['mem_requetes'],1)."% en pondérant par la taille'>{$stats[$what]['nb_hits']} soit ".round(100*$stats[$what]['nb_hits']/$stats[$what]['nb_requetes'],1)."%</td></tr>
		<tr class=tr-0><td class=td-0>Plus vieux cache</td><td>{$stats[$what]['naissance']}</td></tr>";
}
